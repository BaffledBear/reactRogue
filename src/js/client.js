import React from 'react';
import ReactDOM from 'react-dom';

const tileType = {
    WALL: 0,
    FLOOR: 1
};

const tileReverseLookup = [
    "WALL",
    "FLOOR"
];

const inlineList = {
    listStyleType: "none",
    display: "inline"
};

const playerCell = {
    backgroundColor: '#009',
    height: '7px',
    width: '7px'
}

const rowStyle = {
    display: 'inline-block'
}

const weapons = [
    { name: "nothing", type: "weapon", attack: 0 },
    { name: "fists", type: "weapon", attack: 1 },
    { name: "pie", type: "weapon", attack: 2 },
    { name: "stick", type: "weapon", attack: 3 },
    { name: "woodenSword", type: "weapon", attack: 5 },
    { name: "fryingPan", type: "weapon", attack: 8 },
    { name: "sword", type: "weapon", attack: 13 },
    { name: "beamSword", type: "weapon", attack: 21 }
];

function getRandomRoomSize() {
    var size = [];
    size.push(Math.floor(Math.random() * 12) + 5);
    size.push(Math.floor(Math.random() * 12) + 5);
    return size;
}

function getStartingLocation(size) {
    var location = [];
    location.push(Math.floor(Math.random() * size[0]));
    location.push(Math.floor(Math.random() * size[1]));
    return location;
}

function buildMap(width, height) {
    var map = Array.apply(
            null,
            {length: width}
            ).map(
            (cello, oIndex) => {
                var arr = Array.apply(
                    null,
                    {length: height}
                    ).map((celli, iIndex) => {
                        return {
                            type: tileType["WALL"],
                            changed: false
                        }; }
                    );
                return arr;
            });
    var room = getRandomRoomSize();
    var starting = [Math.floor((width / 2) - (room[0] / 2)), Math.floor((height / 2) - (room[0] / 2))];
    var ending = [(+starting[0] + +room[0]), (+starting[1] + +room[1])];
    for (var i = starting[0]; i < ending[0] && i < map.length; i++) {
        for (var j = starting[1]; j < ending[1] && j < map[i].length; j++) {
            map[i][j].type = tileType["FLOOR"];
        }
    }
    return map;
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hp: 100,
            floor: 0,
            weapon: "fists",
            attack: 1,
            xp: 100,
            level: 0,
            damage: 0,
        };
    }

    render(){
        return (
            <div>
                <Info
                hp={this.state.hp}
                level={this.state.level}
                xp={this.state.xp}
                weapon={this.state.weapon}
                attack={this.state.attack}
                floor={this.state.floor}
                damage={this.state.damage} />
                <Map width={this.props.boardSize[0]} height={this.props.boardSize[1]}/>
                <p>
                    This is a roguelike game.
                </p>
            </div>
            );
    }
}

class Info extends React.Component {
    render() {
        return (
            <ul id="infobar">
                <li>HP: {+this.props.hp - +this.props.damage}</li>
                <li>Level: {this.props.level}</li>
                <li>Next Level: {this.props.xp}XP</li>
                <li>Weapon: {this.props.weapon}</li>
                <li>Attack: {this.props.attack}</li>
                <li>Floor: {this.props.floor}</li>
            </ul>
            );
    }
}

class Map extends React.Component {
    constructor(props) {
        super(props);
        var playerLocation = getStartingLocation([this.props.width, this.props.height]);
        var board = buildMap(this.props.width, this.props.height);
        this.state = {
            width: this.props.width,
            height: this.props.height,
            board: board,
            playerLocation: playerLocation
        };
    }

    render() {
        var board = this.state.board.map((arr, hIndex) => {
            var row = arr.map((cell, wIndex) => {
                    var className = (cell.type === 0 ? "tile wall" : "tile floor");
                    if (this.state.playerLocation[0] === hIndex && this.state.playerLocation[1] === wIndex){
                        className = "tile player";
                    }
                    return <Cell
                    key={hIndex+""+wIndex}
                    className={className}
                    location={[wIndex, hIndex]}
                    getState={this.getState.bind(this)} />;
            });
            return <div key={hIndex} className="row" style={rowStyle}>{row}</div>;
        });
        return (<div>{board}</div>);
    }

        componentDidMount() {
        window.addEventListener("keydown", this.handleKeyDown.bind(this));
    }

    handleKeyDown(e) {
        var sTime = Date.now();
        var tBoard = this.state.board.slice(0);
        for (var i = 0; i < tBoard.length; i++) {
            for (var j = 0; j < tBoard[i].length; j++) {
                tBoard[i][j].changed = false;
            }
        }
        var playerLocation = this.state.playerLocation.slice(0);
        var newPlayerLocation = this.state.playerLocation.slice(0);
        tBoard[playerLocation[0]][playerLocation[1]].changed = true;
        switch(e.code) {
            case 'ArrowLeft':
                if (!(newPlayerLocation[0] - 1 < 0)) { newPlayerLocation[0] -= 1; }
                break;
            case 'ArrowRight':
                newPlayerLocation[0] += 1;
                break;
            case 'ArrowUp':
                newPlayerLocation[1] -= 1;
                break;
            case 'ArrowDown':
                newPlayerLocation[1] += 1;
                break;
        }
        tBoard[newPlayerLocation[0]][newPlayerLocation[1]].changed = true;
        this.setState({
            board: tBoard,
            playerLocation: newPlayerLocation
        });
        console.log(this.state.playerLocation);
        console.log(+Date.now() - +sTime);
    }

    getState(location) {
        return this.state.board[location[0]][location[1]];
    }
}

class Cell extends React.Component {

        // shouldComponentUpdate() {
        //     return this.props.changed;
        // }

        render() {
            return (
                <div
                className={this.props.className}></div>
            );
    }
}

ReactDOM.render(
    <Game boardSize={[50, 50]}/>,
    document.getElementById('game')
    );
